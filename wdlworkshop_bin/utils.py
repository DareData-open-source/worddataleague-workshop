import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RandomizedSearchCV
from sklearn.ensemble import RandomForestRegressor
import numpy as np
from numpy.linalg import norm


def plot_histogram(data, variable):
    '''
    Plots histogram for variable on the
    taxi trip duration dataset.

    Arguments:
    - data(pd.DataFrame): DataFrame to plot.
    - variable(str): Variable to plot.

    Returns:
    - none
    '''
    data[variable].hist()
    plt.title('Histogram for Variable {}'.format(variable))
    plt.show()


def remove_outliers(data, variable, stdev_n):
    '''
    Removes outliers from the dataframe using
    the variable and using 3 standards deviation
    to the left and right on the mean.

    Everything on this range will be kept on the
    dataframe - otherwise, we will discard the rows.

    Arguments:
    - data(pd.DataFrame): Dataframe to filter;
    - variable(str): Variable to remove some outliers;
    - stdev_n(int): Number of standard deviations to draw
    the range.

    Returns:
    - filtered_data(pd.DataFrame): Filtered Dataframe that
    only contains data within range of stdevs on variable. 
    '''

    var_mean = data[variable].mean()
    var_std = data[variable].std()

    # Filter upper boundary
    filtered_data = (
        data
        .loc[data[variable] < var_mean + var_std*stdev_n]
    )

    # Filter lower boundary
    filtered_data = (
        filtered_data
        .loc[filtered_data[variable] > var_mean - var_std*stdev_n]
    )

    return filtered_data


def var_x_trip_duration(data, variable):
    '''
    Checks variable vs trip duration mean to
    understand if there's some linear relationship.

    Arguments:
    - data(pd.DataFrame): Contains data for
    with the taxi trip duration info.
    - variable(str): Variable to cross with the
    "trip duration" one.

    Returns:
    - none
    '''
    (
        data
        .groupby([variable], as_index=False)
        ['trip_duration'].mean()
        .plot(kind='scatter', x=variable, y='trip_duration')
    )

    plt.title('Plot of {} vs. Trip Duration Mean'.format(variable))


def create_time_based_features(data):
    '''
    Extracts time based feature from pickup datetime
    of the taxi trip.

    Arguments:
    - data(pd.DataFrame): Data with Taxi Trip Duration.

    Returns:
    - tbf(pd.DataFrame): Dataframe with Time Based Features
    '''

    data.pickup_datetime = pd.to_datetime(data.pickup_datetime)

    month = data.pickup_datetime.dt.month
    day = data.pickup_datetime.dt.day
    weekday = data.pickup_datetime.dt.weekday
    hour = data.pickup_datetime.dt.hour

    tbf = pd.concat(
        [month, day, weekday, hour],
        axis=1
    )

    tbf.columns = ['month', 'day', 'weekday', 'hour']

    return tbf


def build_distance_features(data, distance):
    '''
    Builds distance variable based on "distance".

    Arguments:
    - data(pd.DataFrame): Dataframe with Longitude
    and Latitude points.
    - distance(str): The distance to extract. Atm
    only Euclidean is implemented

    Returns:
    - distance_data(pd.Series): Variable with distance calculated.
    '''

    if distance == 'euclidean':

        distance_data = (
            norm(data.iloc[:, [5, 6]].values - data.iloc[:, [7, 8]], axis=1)
        )

        return distance_data
    else:
        raise NotImplementedError('Distance not implemented yet!')


def plot_distance(data, dist_variable):
    '''
    Plots distance rounded to second decimal
    crossed with trip_duration.

    Arguments:
    - data(pd.DataFrame): Dataframe with distance
    variable.
    - dist_variable(str): Distance variable to round
    and plot.

    Returns:
    - none
    '''
    dist_plot = data.copy()

    dist_plot[dist_variable] = dist_plot[dist_variable].round(2)

    dist_plot = (
        dist_plot.groupby([dist_variable], as_index=False)
        ['trip_duration'].mean()
    )

    var_x_trip_duration(dist_plot, dist_variable)


def plot_map(data, trip_id):
    '''
    Plots "random" taxi trip from the taxi trip
    dataset.

    Use trip_id to select the index of the trip
    you want to plot.

    Arguments:
    - data(pd.DataFrame): Data with the taxi trip
    info.
    - trip_id(int): The index of the trip we want to plot.

    Returns:
    - none
    '''
    ny = plt.imread('../data/map/map.jpg')
    lat_pickup = data.pickup_latitude.iloc[trip_id]
    long_pickup = data.pickup_longitude.iloc[trip_id]
    lat_dropoff = data.dropoff_latitude.iloc[trip_id]
    long_dropoff = data.dropoff_longitude.iloc[trip_id]
    bounding_box = (
        (
            data.pickup_longitude.min(),
            data.pickup_longitude.max(),
            data.pickup_latitude.min(),
            data.pickup_latitude.max())
            )
    fig, ax = plt.subplots(figsize=(20, 20))
    ax.scatter(long_pickup, lat_pickup, zorder=1, c='r', s=500)
    ax.scatter(long_dropoff, lat_dropoff, zorder=1, c='r', s=500)
    ax.set_title('Plotting Random Taxi Trip on New York')
    ax.set_xlim(bounding_box[0], bounding_box[1])
    ax.set_ylim(bounding_box[2], bounding_box[3])
    ax.imshow(ny, zorder=0, extent=bounding_box, aspect='equal')


def get_features_target(data, sample_size, features):
    '''
    Extract features and target for training
    and test dataset.

    Arguments:
    - data(pd.DataFrame): Dataframe with base data.
    - sample_size(int): Sample size for training process.
    Argument used just to speed up training for the workshop.
    - features(list): List of columns to use as features.
    Returns:
    - X_train, X_test, y_train, y_test (pd.DataFrame):
    features and target for training.
    '''
    # Just to reduce time to train in the example
    data_model = data.sample(sample_size)
    features = data_model[features]
    target = data_model['trip_duration']
    X_train, X_test, y_train, y_test = train_test_split(features, target)
    return X_train, X_test, y_train, y_test


def perform_grid_search(model, X_train, y_train):
    # Start Modelling
    if model == 'rf':
        n_estimators = [int(x) for x in np.linspace(start=20, stop=50, num=2)]
        max_depth = [10, 25, 30, 50, 70]
        min_samples_split = [20, 50, 100, 150]
        bootstrap = [True, False]
        random_grid = {'n_estimators': n_estimators,
                       'max_depth': max_depth,
                       'min_samples_split': min_samples_split,
                       'bootstrap': bootstrap}

        rf = RandomForestRegressor()
        model = RandomizedSearchCV(
            estimator=rf,
            param_distributions=random_grid,
            n_iter=10,
            cv=3,
            verbose=10,
            random_state=42,
            n_jobs=2
            )
        # Fit the random search model
        model.fit(X_train, y_train)
    return model
