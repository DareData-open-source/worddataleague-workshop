# Intro

Jupyter notebooks are not ideal for collaborative development.
A lot of issues arise when you want to work collaboratively in a notebook environment - namely: 

-> Lack the ability to use linters;
<br>
-> Prone to git conflicts;
<br>
-> No testing can be performed on code that sits on Notebooks.

Luckily, there's a few workarounds we can do.

This tutorial will give you some hints and tips with some resources you can
download to boost your productivity when coding in Notebooks. This workshop will be a bit generalistic and we'll talk about: 

- Virtual Environments;
- Using VS Code as a IDE;
- Abstracting Functions outside Notebooks;
- Using Linters;
- Writing well documented functions;

This repo is part of the World Data League workshop given by DareData.

## Virtual Environments
There's a high chance that you and your peers will be working on different operating systems 
and with different versions of pythons / libraries.

To manage the different libraries you will be working on, always use a virtual environment
with fixed library versions to avoid issues between you and your peers.

An even better way would be to abstract the operating system (for example, using a docker file)
where you would run your notebook but maybe that's a bit overkill for the task you have
at hand.

Depending on your Python setup, you can use Python's base virtualenv:
- https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/


Or conda virtual environments, if you are using the conda distribution: 
- https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html


The example we will see in the workshop is creating a Python environment on Ubuntu
named sandboxwdl with: 

`python3 -m venv sandboxwdl`

## Activating the Environment
To activate the environment, we can run, on the command line: 

`source sandboxwdl/bin/activate`

We are now working inside our environment. This environment is now an isolated one
that doesn't have the other libraries you already have in our base python. 

After activating the environment on the command line, you will be able to install libraries
inside it.

## Adding the Environment to Jupyter

Wait, but aren't we going to deliver notebooks? 

So how can we access our virtual environment inside our notebooks?

Three main steps to do, after activating the environment: 

- install the library ipykernel using `pip install ipykernel`
- run on the following on the command line: `ipython kernel install --user --name=sandboxwdl`

# Launching Notebook

Let's now launch jupyter from the root of our folder and look at our messy notebook: run 
`jupyter notebooks` and look at the "Messy Notebook - Taxi Trip Duration" file.

Our notebook works "functionally". We can execute the code, train a model, plot some data
and look at the results.

But, it doesn't leverage the usage of functions, the code is not organized and it's hard to read. Also, it will be a pain to version this code.
Let's change, for instance, one of the parameters of the grid search and submit to git.

# Versioning Notebooks

Versioning notebooks is not a great experience. If you change just a single line of code in your notebook, finding the diff is pretty hard and a really painful experience - particularly when you deal with complex changes.

Ideally, code should be abstracted during development - the more code you can put outside of the notebook, the better. This will help to prevent future issues, such as corrupted notebooks and others.

As we've changed a small parameter of our Random Forest grid search, we had to search through an enormous amount of notebook metadata to get to our diff. Imagine this with a longer notebook and even more complex function (and multiple branches.).

Let's look at a cleaner version of our notebook - this one leverages a couple of functions that are abstracted outside of the notebook and available via python scripts.

# Cleaner Notebook

This notebook contains a lot of functions and code that we don't see. Where is it? 
Most code is abstracted in the wdlworkshop_bin folder. How can we load the functions inside our notebook? 

The idea here is to load our functions just like we do with libraries such as pandas, numpy or matplotlib. There are several ways to make this work, the one I find more elegant is using a combination of `setup.py` and `pip install -e.` that installs this folder just like any other python library in our virtual environment (venvs are really important!).

Step by step: 
- add a `__init__.py`- this will tell Python that it should look for code in this folder.
- add a `setup.py` similar to the one the repo. Name the repo whatever you want.
- from the root of the repo, use `pip install -e.` to install the "folder" in your environment. *Don't forget to do this with your virtual environment activated!*

If you do `pip freeze`, you will see something interesting. For instance, in my case, I have one "weird" library: 
- -e git+git@gitlab.com:DareData-open-source/worddataleague-workshop.git@bf7fd6d66ff057bfc02a462e7021e06e19172ed8#egg=wdlworkshop

This "weird" library is the reference to my local code. Python is using this folder to look for code when I'm working on this environment. I can now import function from this folder directly into my notebook.

But.. what if I want to work on my functions and upload them in real time to my notebook? 
We just have to add some auto reload magic to Jupyter: 
`%load_ext autoreload`
`%autoreload 2`

Inside my notebook, I can now import `from wdlworkshop_bin import utils` - this will contain all the code that I want to use inside the notebook.

Let's look at some of the functions we've created for our "Not so Messy Notebook".

# Building Clean Functions

A cool example of how we are improving the readability of our code is checking the "Removing Outliers" part in our Messy Notebook: 

```
print(taxi_trip_duration.shape[0])

taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.pickup_longitude < (taxi_trip_duration.pickup_longitude.mean()+taxi_trip_duration.pickup_longitude.std()*3)]
taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.pickup_longitude > (taxi_trip_duration.pickup_longitude.mean()-taxi_trip_duration.pickup_longitude.std()*3)]

taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.pickup_latitude < (taxi_trip_duration.pickup_latitude.mean()+taxi_trip_duration.pickup_latitude.std()*3)]
taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.pickup_latitude > (taxi_trip_duration.pickup_latitude.mean()-taxi_trip_duration.pickup_latitude.std()*3)]

taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.dropoff_latitude < (taxi_trip_duration.dropoff_latitude.mean()+taxi_trip_duration.dropoff_latitude.std()*3)]
taxi_trip_duration = taxi_trip_duration.loc[taxi_trip_duration.dropoff_latitude > (taxi_trip_duration.dropoff_latitude.mean()-taxi_trip_duration.dropoff_latitude.std()*3)]

print('After removing Outliers')
print(taxi_trip_duration.shape[0])
```

Auch, this code hurts!

Functionally, it works - but debugging this is painful. First, let's avoid repetition by creating a function.

I see three arguments here: data, variable and the number of standard deviations we want to use.
Our function that will substitute this code and that's going to be called on the Notebook: 

```
def remove_outliers(data, variable, stdev_n): 
    '''
    Removes outliers from the dataframe using 
    the variable and using 3 standards deviation 
    to the left and right on the mean.
    
    Everything on this range will be kept on the
    dataframe - otherwise, we will discard the rows.
    
    Arguments: 
    - data(pd.DataFrame): Dataframe to filter;
    - variable(str): Variable to remove some outliers;
    - stdev_n(int): Number of standard deviations to draw
    the range.
    
    Returns:
    - filtered_data(pd.DataFrame): Filtered Dataframe that
    only contains data within range of stdevs on variable.    
    '''
    
    var_mean = data[variable].mean()
    var_std = data[variable].std()
    
    # Filter upper boundary
    filtered_data = (
        data
        .loc[data[variable] < var_mean + var_std*stdev_n]
    )
    
    # Filter lower boundary
    filtered_data = (
        filtered_data
        .loc[filtered_data[variable] > var_mean - var_std*stdev_n]
    )
    
    return filtered_data
```

We've added a docstring with the breakdown of arguments and return elements. Docstrings help others understand what you are trying to achieve with this block of code.

Additionally, notice that I've used `var_mean` and `var_std` to reduce the length of the code line in the filter. Why did I've done this? 

Mostly, to comply with PEP8 guidelines (https://peps.python.org/pep-0008/) - PEP8 is the official style guide for Python code that improves readability and shareability of your code. Do you have to know all the rules for PEP8 by heart? Nope - we also have some automations to do that!

# Installing VSCode Automatic Linter - Flake 8

To replicate the IDE I'm using in the workshop, you have to download VSCode (https://code.visualstudio.com/). There's a chance that you can use another IDE that is able to replicate some of the functionalities.

I've abstracted functions outside of the notebook - but couldn't I develop the functions inside the notebook? I could, but that will be probably less productive when trying to build clean functions for two reasons: 

- Lack of proper versioning, as we've seen.
- Lack of using automatic linter.

We'll focus on the second point right now. Let's first install flake8 on our environment: 
- `pip install flake8`

Then hit: CTRL+Shift+P and "Select Linter". From the dropdown, choose "Flake 8".

If you go to your Python code, you'll have a bunch of new info on syntax warnings. 
Flake 8 will warns us of several issues with our code: 
- Trailling white spaces;
- Syntax Errors;
- Unused objects and libraries;
- Lines of code over 80 characters.

Another cool thing to do inside VS Code is to use a ruler to understand
if our lines our code are too long - check this s.overflow thread to add one:
https://stackoverflow.com/questions/29968499/vertical-rulers-in-visual-studio-code


